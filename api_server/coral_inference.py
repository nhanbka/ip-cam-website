import cv2
import tflite_runtime.interpreter as tflite 
import numpy as np


EDGETPU_LIB = "libedgetpu.so.1"

THRESHOLD = 0.4

def load_labels(path, encoding="utf-8"):
    with open(path, "r", encoding=encoding) as f:
        lines = f.readlines()
        if not lines:
            return {}
        
        if lines[0].split(" ", maxsplit=1)[0].isdigit():
            pairs = [line.split(" ", maxsplit=1) for line in lines]
            return {int(index): label.strip() for index, label in pairs}
        else:
            return {index: line.strip() for index, line in enumerate(lines)} 


def set_input_tensor(interpreter, image):
	# resize while keeping the aspect ratio
	# return the resize ratio for x, y axis
    input_details = interpreter.get_input_details()
    input_shape = input_details[0]["shape"]
    input_h, input_w, input_channel = input_shape[1:]

    h, w, _ = image.shape
    scale = min(input_w / w, input_h / h)  # resize ratio
    w, h = int(w * scale), int(h * scale)

    input_tensor = np.zeros((input_h, input_w, input_channel))
    input_tensor[:h, :w] = np.reshape(
        cv2.resize(image, (w, h), cv2.INTER_AREA),
        (h, w, input_channel)
    )
    input_tensor = np.expand_dims(input_tensor, axis=0)
    interpreter.set_tensor(input_details[0]["index"],
                           np.uint8(input_tensor))

    return scale, scale


def output_tensor(interpreter, i):
    tensor = interpreter.tensor(
        interpreter.get_output_details()[i]["index"]
    )()
    return np.squeeze(tensor)


def get_output_objects(interpreter, threshold, image_scale=(1.0, 1.0)):
	# return list of detected object
    boxes = output_tensor(interpreter, 0)
    class_ids = output_tensor(interpreter, 1)
    scores = output_tensor(interpreter, 2)
    count = int(output_tensor(interpreter, 3))

    height, width = interpreter.get_input_details()[0]["shape"][1:3]
    img_scale_x, img_scale_y = image_scale
    sx, sy = width / img_scale_x, height / img_scale_y

    def make_object(i):
        ymin, xmin, ymax, xmax = boxes[i]
        ymin = int(ymin * sy)
        ymax = int(ymax * sy)
        xmin = int(xmin * sx)
        xmax = int(xmax * sx)
        confidence = scores[i]
        class_id = class_ids[i]
        return [class_id, confidence, xmin, ymin, xmax, ymax]

    return [make_object(i) for i in range(count)
            if scores[i] > threshold]



def setup_interpreter(model):
	interpreter = tflite.Interpreter(
		model_path=model,
		experimental_delegates=[tflite.load_delegate(EDGETPU_LIB, {})]
	)
	interpreter.allocate_tensors()
	return interpreter


def inference(interpreter, frame):
	scale = set_input_tensor(interpreter, frame)

	interpreter.invoke()

	return get_output_objects(interpreter, THRESHOLD, scale)









