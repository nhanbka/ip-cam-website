from threading import Thread
import threading
from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
import subprocess
import os

app = Flask(__name__)
CORS(app, resources={r"/api/*": {"origins": "*"}}) # This will enable CORS for all routes
app.config['CORS_HEADERS'] = 'Content-Type'


blur = 1
isBlob = 'false'

# target_process = "process.py"
target_process = "test.py"

def create(a, b):
    # start with 
    subprocess.Popen(["python3", "/home/pi/api-server/" + target_process, "-i", "/dev/video0", "-o", "/dev/video1", "-b", str(a) ,"-B", str(b)])
    # subprocess.Popen(["python3", "/home/pi/api-server/test.py", "-i", "/dev/video0", "-o", "/dev/video1", "-b", str(a) ,"-B", str(b)])


@app.route('/api/blur', methods=['POST'])
def blurWithParam():
    data = request.get_json()       # get data
    blur = data.get('blur_param', '')
    blob = data.get('blob', '')
    print(blur)
    print(blob)
    process = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    output, error = process.communicate()
    for line in output.splitlines():
        if target_process in str(line):
            processid = str(line).split()[1]
            print(processid)
            os.system("kill -9 " + processid)
    t = threading.Thread(target=create, args=(blur, blob))
    t.start()
    return "OK"


@app.route('/api/findBlob', methods=['POST'])
def findBlob():
    data = request.get_json()       # get data
    blob = data.get('blob', '')
    print(blur)
    print(blob)
    process = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    output, error = process.communicate()
    for line in output.splitlines():
        if target_process in str(line):
            processid = str(line).split()[1]
            print(processid)
            os.system("kill -9 " + processid)
    t = threading.Thread(target=create, args=(blur, blob))
    t.start() 
    return "OK"


if __name__ == '__main__':
    # t = threading.Thread(target=create, args=(1, "false"))
    # t.start()
    create(blur, isBlob)
    print("Init successfully")
    app.run(debug=True, host = '0.0.0.0', port=5000, use_reloader=False)
