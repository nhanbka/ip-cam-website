#! /bin/bash
# Author: nhanlebka

sudo modprobe v4l2loopback devices=2
echo "Init dummy devices"
python3 app.py &
sleep 15
echo "Init /dev/video1"
v4l2-ctl -d /dev/video1 --list-formats
sleep 3
v4l2compress_h264 /dev/video1 /dev/video2 &
sleep 3
v4l2rtspserver -p 22082 /dev/video2 &
