#   import neccessary package
import videostreammodel                             # import user-defined package to support get frame
import numpy as np
import argparse
import imutils
import time
import cv2
from VirtualDevice import virtualdevice               # to output frame to /dev/video1 as a webcam 
import math

from coral_inference import setup_interpreter, inference, load_labels

# Set frame size
# IMG_W = 1024
# IMG_H = 768
IMG_W = 640
IMG_H = 480

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,help = "Path to input video buffer")
ap.add_argument("-o", "--output", required=True, help= "Path to output video buffer")
ap.add_argument("-b", "--blur", required=True, help = "Enter your blur param here")
ap.add_argument("-B", "--blob", required=True, help = "Enter 1 to enable, 0 to disable")
args = vars(ap.parse_args())

# start the file video stream thread and allow the buffer to start to fill
print("[INFO] starting video file thread...")
input_video_path = args["input"]            # always use /dev/video0
output_video_path = args["output"]          # always use /dev/video1
blur_param = int(args["blur"])
find_blob = args["blob"]

 
# interpreter = setup_interpreter("./models/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite")
interpreter = setup_interpreter("./models/ssd_mobilenet_v2_face_quant_postprocess_edgetpu.tflite")

font = cv2.FONT_HERSHEY_SIMPLEX
# names = load_labels("./models/coco_labels.txt")

fvs = videostreammodel.VideoStream(input_video_path).start()
time.sleep(1.0)

# kernel = np.ones((blur_param,blur_param),np.float32)/(blur_param**2)

# init virtual device
virtual_device = virtualdevice.VirtualDevice(output_video_path, IMG_W, IMG_H)
if(blur_param != 1):
    while True:
        # grab the frame from the threaded video file stream
        frame = fvs.read()
        current_time = time.time()

        # process the frame
        if(find_blob == 'true'):
            #Look for faces in the image using the loaded cascade file
            result = inference(interpreter, frame)
            
            for (id, confidence, x,y,x_min,y_min) in result:
                frame[x:x_min, y:y_min] = cv2.blur(frame[x:x_min, y:y_min], (blur_param, blur_param))
                # frame[x:x_min, y:y_min] = cv2.filter2D(frame[x:x_min, y:y_min], -1, kernel)
                # frame[x:x_min, y:y_min] = cv2.medianBlur(frame[x:x_min, y:y_min],blur_param)
                # frame[x:x_min, y:y_min] = cv2.GaussianBlur(frame[x:x_min, y:y_min], (blur_param, blur_param), 0)
                # frame[x:x_min, y:y_min] = cv2.bilateralFilter(frame[x:x_min, y:y_min], blur_param, blur_param << 1, blur_param >> 1)
                # frame[x:x_min, y:y_min] = cv2.sqrBoxFilter(frame[x:x_min, y:y_min], cv2.CV_8U, (blur_param, blur_param))
                # frame[x:x_min, y:y_min] = cv2.boxFilter(frame[x:x_min, y:y_min], cv2.CV_8U, (blur_param, blur_param))
                cv2.rectangle(frame, (x,y), (x_min,y_min), (255,0,0), -1)
                # id = names[id]
                confidence = " {:+.2f}%".format(100*confidence)
                # cv2.putText(frame, str(id), (x,y_min), font, 1, (255,255,0), 1)
                cv2.putText(frame, str(confidence), (x+5,y_min-5), font, 1, (255,255,0), 1)
        else:
            frame = cv2.blur(frame, (blur_param << 1 + 1, blur_param << 1 + 1))
            # frame = cv2.filter2D(frame, -1, kernel)
            # frame = cv2.medianBlur(frame,blur_param)
            # frame = cv2.GaussianBlur(frame, (blur_param, blur_param), 0)
            # frame = cv2.bilateralFilter(frame, blur_param, blur_param << 1, blur_param >> 1)
            # frame = cv2.sqrBoxFilter(frame, cv2.CV_8U, (blur_param, blur_param))
            # frame = cv2.boxFilter(frame, cv2.CV_8U, (blur_param, blur_param))
        cv2.putText(frame, time.ctime(),(10, 30), font, 0.6, (255, 255, 0), 2)
        spend = time.time() - current_time
        cv2.putText(frame, "FPS = {:+.2f}".format(1/spend),(10, 60), font, 0.6, (255, 255, 0), 2)
        
        # export to virtual device
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2YUV)
        virtual_device.schedule_frame(frame)
        # print(spend)
else:
    while True:
        # grab the frame from the threaded video file stream
        frame = fvs.read()
        current_time = time.time()

        # process the frame
        if(find_blob == 'true'):
            #Look for faces in the image using the loaded cascade file
            result = inference(interpreter, frame)
            
            for (id, confidence, x,y,x_min,y_min) in result:
                cv2.rectangle(frame, (x,y), (x_min,y_min), (255,0,0), 1)
                # id = names[id]
                confidence = " {:+.2f}%".format(100*confidence)
                # cv2.putText(frame, str(id), (x,y_min), font, 1, (255,255,0), 1)
                cv2.putText(frame, str(confidence), (x+5,y_min-5), font, 1, (255,255,0), 1)
                
        cv2.putText(frame, time.ctime(),(10, 30), font, 0.6, (255, 255, 0), 2)
        spend = time.time() - current_time
        cv2.putText(frame, "FPS = {:+.2f}".format(1/spend),(10, 60), font, 0.6, (255, 255, 0), 2)
        
        # export to virtual device
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2YUV)
        virtual_device.schedule_frame(frame)
        # print(spend)

cv2.destroyAllWindows()
fvs.stop()