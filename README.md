
#				----  IP CAMERA STREAM WEBSITE   ----

**Requirement** &nbsp;
1. [WebRTCStreamer]([https://github.com/mpromonet/webrtc-streamer/blob/master/README.md](https://github.com/mpromonet/webrtc-streamer/blob/master/README.md))
2. [v4l2rtspserver]([https://github.com/mpromonet/v4l2rtspserver](https://github.com/mpromonet/v4l2rtspserver))
3. OpenCV 4.2

**Introduction** &nbsp;
	This is the website to interact with IP camera. &nbsp;
	
**Step to Install** &nbsp;

- Install Linux Ubuntu 18.04.3 LTS

- Install RTSP Server (v4l2rtspserver) (if stream video from camera):
```bash
sudo apt update && apt install git cmake
git clone https://github.com/mpromonet/v4l2rtspserver.git
cd v4l2rtspserver
cmake . 
sudo make && make install
```
Note: config the port with -p <port> to use the correct output port and need to use with sudo priviliges. For example:
```bash
sudo ./v4l2rtspserver -p 22082 <device>
```
(do not need to specify /dev/video0 as it is used by default)
- Install webrtcstreamer
```bash
git clone https://github.com/mpromonet/webrtc-streamer/releases/download/v0.2.6/webrtc-streamer-v0.2.6-Linux-armv7l-Release.tar.gz 	(đối với Raspberry Pi)
git clone https://github.com/mpromonet/webrtc-streamer/releases/download/v0.2.6/webrtc-streamer-v0.2.6-Linux-x86_64-Release.tar.gz	(đối với Ubuntu)
```
To use:
```bash
./webrtc-streamer [-H http port] [-S[embeded stun address]] -[v[v]]  [url1]...[urln]
./webrtc-streamer [-H http port] [-s[external stun address]] -[v[v]] [url1]...[urln]
./webrtc-streamer -V
    	-v[v[v]]           : verbosity
    	-V                 : print version

    	-H [hostname:]port : HTTP server binding (default 0.0.0.0:8000)
		-w webroot         : path to get files
		-c sslkeycert      : path to private key and certificate for HTTPS
		-N nbthreads       : number of threads for HTTP server
		-A passwd          : password file for HTTP server access
		-D authDomain      : authentication domain for HTTP server access (default:mydomain.com)

	-S[stun_address]                   : start embeded STUN server bind to address (default 0.0.0.0:3478)
	-s[stun_address]                   : use an external STUN server (default:stun.l.google.com:19302 , -:means no STUN)
	-t[username:password@]turn_address : use an external TURN relay server (default:disabled)
	-T[username:password@]turn_address : start embeded TURN server (default:disabled)
	
    -a[audio layer]    : spefify audio capture layer to use (default:3)		

	-n name -u videourl -U audiourl : register a name for a video url and an audio url
     	[url]                           : url to register in the source list
	-C config.json                  : load urls from JSON config file 
```
- Install v4l2loopback
```bash
sudo apt-get install v4l2loopback-utils
```
To use
```bash
sudo modprobe v4l2loopback devices=2
```
- Install python 3.6.9:
```bash
sudo apt install python3
```
 Then check with “python –version” command to make sure you’re using python 3.6.9. If the default python version is python 2.x, change it:
```bash
sudo update-alternatives --install /usr/bin/python python \
/usr/bin/python3.6 1
```
- Install opencv-python:
```bash
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
cd ~/<my_working_directory>
git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git
cd ~/opencv
mkdir build
cd build
make -j7 # runs 7 jobs in parallel
sudo make install
```
- Make sure to install libssl
```bash
sudo apt-get install libssl-dev
```
- Install RTSP Server (Live555MediaServer) (if stream static video)
```bash
cd $HOME
wget http://www.live555.com/liveMedia/public/live555-latest.tar.gz
tar xvf live555-latest.tar.gz
cd live
```
If you want the rtsp server does not transfer through 8554 by default, do this
```bash
cd mediaServer
sudo vim live555MediaServer.cpp
```
And then find rtspServerPortNum, change them its value from 8554 to whatever port you want

```bash
./genMakefiles linux
sudo make -j4
sudo make install
```
To run:
```bash
cd live
live555MediaServer
```
- Install v4l2tools (make sure to install the version with v4l2compress_h264)
```bash
git clone https://github.com/mpromonet/v4l2tools.git
cd v4l2tools
git checkout 43c171bb75 (I tested with this commit)
sudo make -j4 (optional, suggest "4" for Raspberry Pi 4 for the best speed)
sudo make install
```
- Install Flask, flask_cors
```bash
pip3 install
```

**Run**
In webrtc-streamer, config config.json to read "rtsp:///(your-video-ip):22082/unicast

Replace html in webrtc-streamer with http-server folder
(this bash below will work in the future)
```bash
sudo modprobe v4l2loopback devices=2
sudo python3 api-server/process.py -i /dev/video0 -o /dev/video1 -b 1 -B true (may different depend on your device)
<-i: specify input; -o: specify output; -b: specify blur kernel size; -B: allow finding blob>
sudo v4l2compress_h264 /dev/video1 /dev/video2 (/dev/video1 is output of the above command)
sudo v4l2rtspserver -p 22082 /dev/video2 (/dev/video2 is output of the above command)
sudo ./webrtc-streamer/webrtc-streamer -H 0.0.0.0:<port> -a -C config.json
```
(current bash will work)
```bash
sudo ./v4l2rtspserver/v4l2rtspserver -p 22082 /dev/video0 &
sudo ./webrtc-streamer/webrtc-streamer -H 0.0.0.0:<port> -a -C config.json
```
Open http://<your ip address>:<port>/services.html to enjoy the video.

**Reference**
- https://kevinsaye.wordpress.com/2018/10/17/making-a-rtsp-server-out-of-a-raspberry-pi-in-15-minutes-or-less/
- [https://github.com/mpromonet/webrtc-streamer](https://github.com/mpromonet/webrtc-streamer)