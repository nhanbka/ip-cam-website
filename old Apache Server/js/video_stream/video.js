var scrollStatPl = true;
var scrollStatWs = true;
var pllogs = document.getElementById("pllogs");
var wslogs = document.getElementById("wslogs");

var player_src = "";
if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
    player_src = "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov";
} else {
    player_src = "rtsp://127.0.0.1:8554/test/";
}

var console = (function(oldConsole) {
    return {
        log: function() {
            oldConsole.log(newConsole(arguments, "black", "#A9F5A9"));
        },
        info: function() {
            oldConsole.info(newConsole(arguments, "black", "#A9F5A9"));
        },
        warn: function() {
            oldConsole.warn(newConsole(arguments, "black", "#F3F781"));
        },
        error: function() {
            oldConsole.error(newConsole(arguments, "black", "#F5A9A9"));
        }
    };
}(window.console));

function newConsole(args, textColor, backColor) {
    let text = '';
    let node = document.createElement("div");
    for (let arg in args) {
        text += ' ' + args[arg];
    }
    node.appendChild(document.createTextNode(text));
    node.style.color = textColor;
    node.style.backgroundColor = backColor;
    pllogs.appendChild(node);
    autoscroll(pllogs);
    return text;
}

//Then redefine the old console
window.console = console;

function cleanLog(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function autoscroll(element) {
    if (scrollStatus(element)) {
        element.scrollTop = element.scrollHeight;
    }
    if (element.childElementCount > 1000) {
        element.removeChild(element.firstChild);
    }
}

function scrollset(element, state) {
    if (state) {
        element.scrollTop = 0;
        scrollChange(element, false);
    } else {
        element.scrollTop = element.scrollHeight;
        scrollChange(element, true);
    }
}

function scrollswitch(element) {
    if (scrollStatus(element)) {
        scrollChange(element, false);
    } else {
        scrollChange(element, true);
    }
}

function scrollChange(element, status) {
    if (scrollStatus(element)) {
        scrollStatPl = false;
        document.getElementById("scrollSetPl").innerText = "Scroll on";
    } else {
        scrollStatPl = true;
        document.getElementById("scrollSetPl").innerText = "Scroll off";
    }
}

function scrollStatus(element) {
    if (element.id === "pllogs") {
        return scrollStatPl;
    } else {
        return scrollStatWs;
    }
}


var player = document.getElementById('test_video');
var rtspsource = document.createElement("source");
rtspsource.src = player_src;
player.appendChild(rtspsource);
Streamedian.player('test_video', { socket: "wss://streamedian.com/ws/" });
var range = document.getElementById('rate');
var set_live = document.getElementById('to_end');
var range_out = document.getElementById('rate_res');
range.addEventListener('input', function() {
    player.playbackRate = range.value;
    range_out.innerHTML = 'x${range.value}';
});
set_live.addEventListener('click', function() {
    range.value = 1.0;
    range_out.innerHTML = 'live';
    player.playbackRate = 1;
    player.currentTime = player.buffered.end(0); //player.seekable.end(player.seekable.length - 1);
});