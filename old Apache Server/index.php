        <?php include('include/header.php') ?>
        <div id="header-wrapper" class="header-slider">
            <header class="clearfix">
                <div class="logo">
                    <img src="img/logo_190-125.png" alt="" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <div id="main-flexslider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <p class="home-slide-content">
                                            <strong>smoothly</strong> and friendly
                                        </p>
                                    </li>
                                    <li>
                                        <p class="home-slide-content">
                                            Easy to <strong>use</strong>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="home-slide-content">
                                            We loves <strong>simplicity</strong>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <!-- end slider -->
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- section: team -->
        <section id="about" class="section">
            <div class="container">
                <h4>Who We Are</h4>
                <div class="row">
                    <div class="span4 offset1">
                        <div>
                            <h2>We live with <strong>creativity</strong></h2>
                            <p>
                                <br><br>
                                We have worked together since Feb, 10th 2020.<br>
                                We now have 11 people.
                            </p>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="aligncenter">
                            <img src="img/icons/creativity.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </section>
        <!-- end section: team -->

        <footer>
            <div class="container">
                <div class="row">
                    <div class="span6 offset3">
                        <ul class="social-networks">
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- ./container -->
        </footer>

        <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
        <script src="js/jquery.js"></script>
        <script src="js/jquery.scrollTo.js"></script>
        <script src="js/jquery.nav.js"></script>
        <script src="js/jquery.localScroll.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/isotope.js"></script>
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/inview.js"></script>
        <script src="js/animate.js"></script>
        <script src="js/custom.js"></script>
        <script src="contactform/contactform.js"></script>

    </body>

</html>