<?php
    /* Display error log */
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    /*
    IMPORT PHP-OPENCV FUNCTION, CONST, FILE
    */
    use CV\{ Mat, Scalar, Point, Size };
    use function CV\{ imwrite, imread, blur, add };
    use const CV\{ IMREAD_COLOR };

    define('UPLOAD_DIR', 'images/');
    define('SENDTOCLIENT_DIR', 'images/sendToClient/');

    function uploadImage($img, $name){
    // upload canvas image to server
        $img = str_replace("data:image/png;base64,", "", $img);
        $img = str_replace(" ", "+", $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $name . ".png";
        $success = file_put_contents($file, $data);
        return $success;
    }

    function blurImage($kernel_size, $src){
    // blur received image
        $MAX_KERNEL_LENGTH = $kernel_size;
        $data = imread($src, IMREAD_COLOR);
        $dst = Mat::zerosBySize($data->size(), $data->type());
        for ($i = 1; $i < $MAX_KERNEL_LENGTH + 1; $i = $i + 2) {
            blur($data, $dst, new Size($i, $i), new Point(-1, -1));
        }
        return $dst;
    }

    function adjustBrightness($src, $amount){
    // control the brightness
        $dst = Mat::zerosBySize($data->size(), $data->type());
        add($src, new Scalar());
    }
    
    function processImage($postData){
        $img = $postData["photo"];
        $name = $postData["name"];
        $kernel_size = (int)$postData['blur_param'];
        $success = uploadImage($img, $name);
        if($success){
            print("Upload Success");
            $edit_image = blurImage($kernel_size, UPLOAD_DIR . $name . ".png");
            imwrite(SENDTOCLIENT_DIR.$name.".png", $edit_image);
            // $myfile = fopen("auto.sh", "w+") or die("Unable to open file!");
            // fwrite($myfile, "python3 process.py " . UPLOAD_DIR . $name . ".png ". $kernel_size . " >> nhan.log");
            // fclose($myfile);
            // shell_exec("./auto.sh >> nhan.log");
        }
        else {
            print("Upload False");
        }
    }
    

    // choose function to execute
    if(isset($_POST) && $_POST){
        if($_POST['call'] === 'processImage')
            processImage($_POST);
    }

?>


    