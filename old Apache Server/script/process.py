
import numpy as np
import cv2 as cv
import sys

filename, blur_param = sys.argv[1], int(sys.argv[2])
img = cv.imread('images/' + filename)
blur = cv.blur( img, ( blur_param, blur_param))
cv.imwrite('images/sendToClient/' + filename, blur_param)
