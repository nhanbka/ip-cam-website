        <?php include('include/header.php') ?>

        <section id="video" class="section">
            <div class="container" id="display-video">
                <h4>VIDEO FROM IP CAMERA</h4>
                <div class="left">
                    <br>
                    <button class="btn btn-rounded" id="snap-btn" onclick="snap_1()">SNAP</button><br>
                    <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1"> FIND BLOB
                    </label><br>
                    <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox2" value="option2" onclick="blurParam()"> BLUR<br>
                        <div class="blur-param">
                            <input type="radio" name="blur_parameter" value="1" disabled checked> 1
                            <input type="radio" name="blur_parameter" value="3" disabled> 3
                            <input type="radio" name="blur_parameter" value="5" disabled> 5
                            <input type="radio" name="blur_parameter" value="7" disabled> 7
                            <input type="radio" name="blur_parameter" value="9" disabled> 9
                        </div>
                    </label>
                    <br><br>
                    <button class="btn btn-rounded" id="live-btn">LIVE</button><br><br><br><br>
                    <a href="/test.php" style="text-decoration: none"><button class="btn btn-rounded" id="set_param-btn">SET PARAM</button><br><br></a>
                    <div class="shutter-text">Shutter Speed</div><br>
                    <select id="shutter" name="shutter_speed">
                        <option value="0.5">0.5x</option>
                        <option value="1" selected>Live</option>
                        <option value="1.5">1.5x</option>
                        <option value="2">2x</option>
                        <option value="2.5">2.5x</option>
                    </select>
                    <br><br>
                    <div class="brightness-text">Brightness</div><br>
                    <input id="brightness" class="input" type="range" min="0.25" max="2.0" value="1.0" step="0.05">
                    <!-- <input type="text" placeholder="Gain" id="gain"> -->
                </div>
                <div class="right">
                    <video id="test_video" controls autoplay preload="auto"></video>
                    <canvas id="canvas" style="overflow:auto; display:none"></canvas>
                    <a id="download"></a>
                </div>
            </div>
        </section>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="span6 offset3">
                        <ul class="social-networks">
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
                            <li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- ./container -->
        </footer>

        <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
        <script src="js/video_stream/jquery-1.9.1.js"></script>
        <script src="js/video_stream/free.player.1.8.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/jquery.scrollTo.js"></script>
        <script src="js/jquery.nav.js"></script>
        <script src="js/jquery.localScroll.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/isotope.js"></script>
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/inview.js"></script>
        <script src="js/animate.js"></script>
        <script src="js/custom.js"></script>
        <script src="contactform/contactform.js"></script>
        <script>
            function blurParam() {
                var checkBox = document.getElementById("inlineCheckbox2");
                var radioOption = document.getElementsByName("blur_parameter");
                var i = 0;
                if(checkBox.checked == true)
                    for(;radioOption[i]; i++)
                        radioOption[i].disabled = false;
                else for(;radioOption[i]; i++)
                        radioOption[i].disabled = true;
            }
        </script>

        <!-- 
        -- Handle Video Stream Player 
        --
        --
        !-->
        <script>
            var player_src = "";
            if(/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
                player_src = "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov";
            } else {
                player_src = "rtsp://127.0.0.1:8554/test/";
            }

        </script>

        <script>
            var player = document.getElementById('test_video');
            var rtspsource = document.createElement("source");
            var shutter = document.getElementById("shutter");
            var brightness = document.getElementById("brightness");
            rtspsource.src=player_src;
            player.appendChild(rtspsource);
            Streamedian.player('test_video', {socket:"wss://streamedian.com/ws/"});
            var set_live = document.getElementById('live-btn');

            set_live.addEventListener('click', function () {
                player.currentTime = player.buffered.end(0);
                player.playbackRate = 1;
                shutter.value = 1;
                player.setAttribute("style", "filter: brightness(1)");
                brightness.value = 1;
                player.play();
            });
            
            shutter.addEventListener('change', function(){
                player.playbackRate = parseFloat(shutter.value);
            });

            brightness.addEventListener('input', function(){
                player.setAttribute("style", "filter: brightness("+ brightness.value + ")")
            });

            player.addEventListener('seeked', function(){
                if(player.currentTime === player.duration){
                    player.currentTime = 0;
                }
                player.load(manifestUri, player.currentTime);1
            });

        </script>

        <script>
            function snap_1(){
                var canvas = document.getElementById('canvas');
                var video = document.getElementById('test_video');
                var brightness = document.getElementById("brightness");

                // for drawing the video element on the canvas
                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;
                canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
                
                var image = canvas.toDataURL();
                var amount = brightness.value * 100 - 100;
                $blur_option = $('input[name="blur_parameter"]:checked').val();
                $fileName = "img" + Date.now();
                $.ajax({
                    type: 'POST',
                    timeout: 50000,
                    url: '/script/capture.php',
                    data: {
                        photo: image,
                        name: $fileName,
                        call: 'processImage',
                        blur_param: parseInt($blur_option, 10),
                        // brightness: amount
                    }
                }).done(function(o) {
                            // download Image to desktop
                            var dataURL = 'script/images/sendToClient/' + $fileName + '.png';
                            var download = document.getElementById('download');
                            download.href = dataURL;
                            download.download = "./BlurImage" + Date.now() + ".png";
                            download.click();
                        });
            }
        </script>
    </body>

</html>