<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <?php if($_SERVER['PHP_SELF'] === "/services.php") {?>
    <title>Camera Display</title>
    <?php } else {?>
    <title>MiTech Camera Tracking</title>
    <?php } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="color/default.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/camera.png">
    <?php if ($_SERVER['PHP_SELF'] === "/services.php"){?>
    <link href="css/services-style.css" rel="stylesheet">
    <?php }  ?>
</head>

<body>
    <!-- navbar -->
    <div class="navbar-wrapper">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <!-- Responsive navbar -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </a>
                    <h1 class="brand"><a href="http://mitech.jp">MiTech</a></h1>
                    <!-- navigation -->
                    <nav class="pull-right nav-collapse collapse">
                        <ul id="menu-main" class="nav">
                            <li><a title="home" href="/">Home</a></li>
                            <li><a title="about" href="/#about">About</a></li>
                            <li><a title="services" href="/services.php">Services</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Header area -->