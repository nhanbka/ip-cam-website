!/bin/bash
# Author: nhanlebka

sudo apt-get update
sudo apt-get install git cmake -y

sudo apt-get install v4l2loopback-utils -y
sudo apt install python3 libssl-dev -y
sudo apt-get install python3-pip -y
sudo apt-get install build-essential -y
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev -y
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev -y
sudo apt-get install libjasper-dev libdc1394-22-dev -y

# install v4l2tools
cd tools
sudo make -j4 (optional, suggest "4" for Raspberry Pi 4 for the best speed)
sudo make install

# install v4l2rtspserver.git
cd ../v4l2rtspserver
cmake . 
sudo make
sudo make install

# install for api server
cd ..
sudo pip3 install flask
sudo pip3 install -U flask_cors

# install webrtc server
cd ..
git clone https://github.com/mpromonet/webrtc-streamer/releases/download/v0.2.6/webrtc-streamer-v0.2.6-Linux-armv7l-Release.tar.gz
tar -xvf webrtc-streamer-v0.2.6-Linux-armv7l-Release.tar.gz
rm -rf ./webrtc-streamer-v0.2.6-Linux-armv7l-Release/html
mv -r ./002_smart_camera_web_client/httpServer ./webrtc-streamer-v0.2.6-Linux-armv7l-Release/html

